/*
树（Tree）是n（n≥0）个结点的有限集。n=0时称为空树。在任意一
棵非空树中：（1）有且仅有一个特定的称为根（Root）的结点；
（2）当n＞1时，其余结点可分为m（m＞0）个互不相交的有限集
T1、T2、……、Tm，其中每一个集合本身又是一棵树，并且称为根的
子树（SubTree）
*/



/*
ADT 树(tree)
Data
    树是由一个根结点和若干棵子树构成。树中结点具有相同数据类型及层次关系。
	Operation
    InitTree(*T):               构造空树T。
    DestroyTree(*T):            销毁树T。
    CreateTree(*T, definition): 按definition中给出树的定义来构造树。
    ClearTree(*T):              若树T存在，则将树T清为空树。
    TreeEmpty(T):               若T为空树，返回true，否则返回false。
    TreeDepth(T):               返回T的深度。
    Root(T):                    返回T的根结点。
    Value(T, cur_e):            cur_e是树T中一个结点，返回此结点的值。
    Assign(T, cur_e, value):    给树T的结点cur_e赋值为value。
    Parent(T, cur_e):           若cur_e是树T的非根结点，则返回它的双亲，否则返回空。
    LeftChild(T, cur_e):        若cur_e是树T的非叶结点，则返回它的最左孩子，否则返回空。
    RightSibling(T, cur_e):     若cur_e有右兄弟，则返回它的右兄弟，否则返回空。
    InsertChild(*T, *p, i, c):  其中p指向树T的某个结点，i为所指结点p的度加上1，
								非空树c与T不相交，操作结果为插入c为树T中p指结点的第i棵子树。
    DeleteChild(*T, *p, i):     其中p指向树T的某个结点，i为所指结点p的度，
								操作结果为删除T中p所指结点的第i棵子树。
endADT
*/



/* 树的双亲表示法结点结构定义 */
#define MAX_TREE_SIZE 100
/* 树结点的数据类型，目前暂定为整型 */
typedef int TElemType;
/* 结点结构 */
typedef struct PTNode {
	/* 结点数据 */
	TElemType data;
	/* 双亲位置 */
	int parent;
} PTNode;
/* 树结构 */
typedef struct {
	/* 结点数组 */
	PTNode nodes[MAX_TREE_SIZE];
	/* 根的位置和结点数 */
	int r, n;
} PTree;