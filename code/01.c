/* 剑指 Offer 58 - II. 左旋转字符串 */
#include<stdio.h>
#include<string.h>
#include <stdlib.h>

char* reverseLeftWords(char* s, int n){
    int i, len;
    len = strlen(s);
    char *buf=calloc(len+1,1);
    if(len>10000)
        return NULL;
    if(!(1 <= n && n < len))
        return NULL;
    
    for ( i = 0; i < (len-n); i++)
    {
        buf[i] = s[i+n];
    }

    for ( i = 0; i < n; i++)
    {
        buf[i+len-n] = s[i];
    }

    return buf;
}

int main(int argc, char const *argv[])
{
    char *s = "abcdefg";
    printf("%s\r\n", reverseLeftWords(s, 2));
    return 0;
}
