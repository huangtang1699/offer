/* 输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有奇数位于数组的前半部分，所有偶数位于数组的后半部分。 */
#include<stdio.h>
#include<stdlib.h>

int* exchange(int* nums, int numsSize, int* returnSize);
void printnums(int *nums, int numsSize);

int main(int argc, char const *argv[])
{
    /* code */
    int nums[] = {1, 2, 3, 4};
    int *arr;
    int returnSize;
    arr = exchange(nums, 4, &returnSize);
    printnums(arr, returnSize);
    free(arr);
    return 0;
}

int* exchange(int* nums, int numsSize, int* returnSize){

    int *arr;
    int odd = 0;
    int even = numsSize - 1;

    arr = malloc(numsSize*sizeof(int));
    
    for (int i = 0; i < numsSize; i++)
    {
        if(nums[i]%2 == 0)
        {
            arr[even--] = nums[i];
        }
        else
        {
            arr[odd++] = nums[i];
        }
    }

    *returnSize = numsSize;

    return arr;
}

void printnums(int *nums, int numsSize)
{
    for (int i = 0; i < numsSize; i++) {
        printf("%d ", nums[i]);
    }
    printf("\n");
}
