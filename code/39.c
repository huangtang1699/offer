/* 剑指 Offer 39. 数组中出现次数超过一半的数字 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void bubbleSort(int *arr, int len);
void printarr(int* nums, int numsSize);
int majorityElement(int* nums, int numsSize){

    int* arr;
    int temp = 0;
    int count = 0;

    arr = malloc(sizeof(int)*numsSize);

    memcpy(arr, nums, sizeof(int)*numsSize);
    bubbleSort(arr, numsSize);

    return arr[numsSize/2];    
}

void printarr(int* nums, int numsSize)
{
    for (int i = 0; i < numsSize; i++)
    {
        printf("%d, ", nums[i]);
    }
    printf("\r\n");
}
int main(int argc, char const *argv[])
{
    int nums[] = {1, 2, 3, 2, 2, 2, 5, 4, 2};
    printf("hello world\r\n");
    printf("出现次数超过一半的数字是： %d\r\n", majorityElement(nums, 9));
    return 0;
}

void swap(int *pa,int *pb) {
    int temp;
    temp=*pa;
    *pa=*pb;
    *pb=temp;
}

void bubbleSort(int *a,int n)
{
    int i,j;
    for(i=0; i<n-1; i++) {
        for(j=0; j<n-i-1; j++) {
            if(a[j]>a[j+1]) {
                swap(a+j,a+j+1);
            }
        }
    }
}