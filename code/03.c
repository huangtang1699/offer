/* 剑指 Offer 56 - II. 数组中数字出现的次数 II */
#include<stdio.h>
#include <math.h>

#define BIT_MAX 32
#define DEBUG 1

void printnums(int *nums)
{
#if DEBUG
    for (int i = 0; i < BIT_MAX; i++) {
        printf("%d ", nums[i]);
    }
    printf("\n");
#endif
}

// 二进制转十进制
int binary_to_decimal(int *p, int len)
{
    int decimal = 0;

    for (int i = 0; i < len; i++) {
        if (p[i] % 3 == 1) {
            decimal += (p[i] % 3) * (int)pow(2.0, i);
        }
    }

    return decimal;
}

int singleNumber(int *nums, int numsSize)
{
    int i, j;
    int bit_arr[BIT_MAX] = {0};
    unsigned int bit_mask;

    for (i = 0; i < numsSize; i++) {
        bit_mask = 1;
        for (j = 0; j < BIT_MAX; j++) {
            if (nums[i] & bit_mask) {
                bit_arr[j] += 1;
            }
            bit_mask <<= 1;    
        }
        printnums(bit_arr);
    }

    return binary_to_decimal(bit_arr, BIT_MAX);
}


/*
int singleNumber(int* nums, int numsSize){
    int num[21474] = {};
    int i = 0;
    for(i=0;i<numsSize;i++)
    {
        num[nums[i]]++;
    }

    i = 0;
    while(!(1==num[i++]));
    
    return i-1;
}
*/
int main(int argc, char const *argv[])
{
    int nums1[] = {4, 4, 1, 6};
    int nums2[1000] = {};
    int i=0;
    for(i=0;i<1000;i++)
        nums2[i]=7;
    
    nums2[0]=9;
    printf("1:%d\r\n", singleNumber(nums1,4));
    //printf("2:%d\r\n", singleNumber(nums2,1000));

    return 0;
}
