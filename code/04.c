/* 在一个长度为 n 的数组 nums 里的所有数字都在 0～n-1 的范围内。数组中某些数字是重复的，但不知道有几个数字重复了，也不知道每个数字重复了几次。请找出数组中任意一个重复的数字。*/
#include<stdio.h>
#include <string.h>
#include <stdlib.h>

int findRepeatNumber(int* nums, int numsSize);

int main(int argc, char const *argv[])
{
    int nums[]={2, 3, 1, 0, 2, 5, 3};
    printf("repeat num :%d\r\n", findRepeatNumber(nums, 7));
    return 0;
}

int findRepeatNumber(int* nums, int numsSize)
{
    int *hash = (int *)calloc(numsSize, sizeof(int));
    for(int i = 0; i < numsSize; i++){
        if(hash[nums[i]] == 1){
            return nums[i];
        } else {
            hash[nums[i]]++;
        }
    }
    return -1;
}