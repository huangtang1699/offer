/* 剑指 Offer 15. 二进制中1的个数 */
#include<stdio.h>

typedef  unsigned int uint32_t;

// int hammingWeight(uint32_t n) {
//     uint32_t num=n;
//     int onenums = 0;
//     for (int i = 0; i < 32; i++)
//     {
//         num=n>>i;
//         if(num&0x01)
//             onenums++;
//     }

//     return onenums;
    
// }

//去掉一个数n中的最后一位二进制位上的1的操作：n = n & (n - 1)
int hammingWeight(uint32_t n) {
    int res = 0;

    while (n != 0) {
        n = n & (n - 1);
        printf("%d\r\n",n);
        res++;
    }
    printf("\r\n");
    return res;
}

/*
7:0000 0111
6:0000 0110
4:0000 0100
*/

int main(int argc, char const *argv[])
{
    printf("%d\r\n", hammingWeight(7));
    return 0;
}

