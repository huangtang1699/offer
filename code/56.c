/* 剑指 Offer 56 - I. 数组中数字出现的次数 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*
    1.0和任何数异或等于该数
    2.任何数和自身异或等于0
    3.异或运算满足交换律
*/
#define NUM_MAX 2

int find_first_bit1(int num)
{
    int index = 0;

    while ((num & 1) == 0 && index <= 32) {
        num >>= 1;
        index++;
    }

    return index;
}

int is_bit1(int num, int index)
{
    num >>= index;
    return (num & 1);
}

int *singleNumbers(int *nums, int numsSize, int *returnSize)
{
    int i;
    int xor_value = 0;
    int index;

    int* g_arr=(int*)malloc(sizeof(int)*2);

    memset(g_arr, 0, sizeof(int) * 2);

    //将数组中所有数据整体异或，得到xor_value
    for (i = 0; i < numsSize; i++) {
        xor_value ^= nums[i];
    }

    //找到xor_value 2进制第一个值为1的位index
    index = find_first_bit1(xor_value);

    //根据上面得到的位，对数组进行分组异或，最后得到两个值
    for (i = 0; i < numsSize; i++) {
        if (is_bit1(nums[i], index)) {
            g_arr[0] ^= nums[i];
        } else {
            g_arr[1] ^= nums[i];
        }
    }

    *returnSize = 2;
    return &g_arr[0];
}

int main(int argc, char const *argv[])
{
    int num[] = {4, 1, 4, 6};
    int *num2;
    int returnsize = 0;
    num2 = singleNumbers(num, 4, &returnsize);
    free(num2);
    return 0;
}
