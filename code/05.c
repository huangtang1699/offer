/* 剑指 Offer 05. 替换空格 */
#include<stdio.h>
#include<string.h>
#include <stdlib.h>

char* replaceSpace(char* s){
/*
1.获取字符串长度
2.遍历获取空格的个数
3.根据1，2申请空间
4.将字符串逐字节复制到新空间内，复制时遇到空格用"%20"代替
5.返回新字符串
*/
    int slen = strlen(s);
    int spacenum = 0;
    int i = 0;
    int j = 0;
    for ( i = 0; i < slen; i++)
    {
        if(s[i]==' ')
            spacenum++;
    }

    char *newStr = malloc(slen + spacenum*2 + 1);

    for ( i = 0; i < slen; i++)
    {
        if(s[i]==' ')
        {
            newStr[j++]='%';
            newStr[j++]='2';
            newStr[j++]='0';
        }
        else
        {
            newStr[j++]=s[i];
        }
    }
    newStr[j]='\0';

    return newStr;
}

int main(int argc, char const *argv[])
{
    char *s = "We are happy.";
    
    printf("%s\r\n",replaceSpace(s));
    return 0;
}
