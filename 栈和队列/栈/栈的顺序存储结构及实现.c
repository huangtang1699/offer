/*
栈是限定仅在表尾进行插入和删除操作的线性表。
队列是只允许在一端进行插入操作、而在另一端进行删除操作的线性表
*/
/*允许插入和删除的一端称为栈顶（top），另一端称为栈底（bottom），不含任何数据元素的栈称为空栈。
栈又称为后进先出（LastIn First Out）的线性表，简称LIFO结构。
*/
//栈的插入操作，叫作进栈，也称压栈、入栈。
//栈的删除操作，叫作出栈，也有的叫作弹栈。

/*
ADT 栈(stack)
Data
同线性表。元素具有相同的类型，相邻元素具有前驱和后继关系.
Operation
InitStack(*S): 初始化操作，建立一个空栈S。
DestroyStack(*S): 若栈存在，则销毁它。
ClearStack(*S): 将栈清空。
StackEmpty(S): 若栈为空，返回true，否则返回false。
GetTop(S, *e): 若栈存在且非空，用e返回S的栈顶元素。
Push(*S, e): 若栈S存在，插入新元素e到栈S中并成为栈顶元素。
Pop(*S, *e): 删除栈S中栈顶元素，并用e返回其值。
StackLength(S): 返回栈S的元素个数。
endADT
*/

/* SElemType类型根据实际情况而定，这里假设为int */
typedef int SElemType;
typedef struct {
	SElemType data[MAXSIZE];
	/* 用于栈顶指针 */
	int top;
} SqStack;

/* 插入元素e为新的栈顶元素 */
Status Push(SqStack *S, SElemType e)
{
	/* 栈满 */
	if (S->top == MAXSIZE - 1) {
		return ERROR;
	}
	/* 栈顶指针增加一 */
	S->top++;
	/* 将新插入元素赋值给栈顶空间 */
	S->data[S->top] = e;
	return OK;
}



/* 若栈不空，则删除S的栈顶元素，用e返回其值，
并返回OK；否则返回ERROR */
Status Pop(SqStack *S, SElemType *e)
{
	if (S->top == -1)
		return ERROR;
	/* 将要删除的栈顶元素赋值给e */
	*e = S->data[S->top];
	/* 栈顶指针减一 */
	S->top--;
	return OK;
}
