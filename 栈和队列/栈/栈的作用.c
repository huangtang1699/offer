/* 斐波那契的递归函数 */
int Fbi(int i)
{
	if (i < 2)
		return i == 0 ? 0 : 1;
	/* 这里Fbi就是函数自己，它在调用自己 */
	return Fbi(i - 1) + Fbi(i - 2);
}

/* 逆波兰计算法 */
