typedef struct StackNode {
	SElemType data;
	struct StackNode *next;
} StackNode, *LinkStackPtr;

typedef struct LinkStack {
	LinkStackPtr top;
	int count;
} LinkStack;

/* 插入元素e为新的栈顶元素 */
Status Push(LinkStack *S, SElemType e)
{
	LinkStackPtr s
	    = (LinkStackPtr)malloc(sizeof(StackNode));
	s->data = e;
	/* 把当前的栈顶元素赋值给新结点的直接后继，如图中① */
	s->next = S->top;
	/* 将新的结点s赋值给栈顶指针，如图中② */
	S->top = s;
	S->count++;
	return OK;
}

/* 若栈不空，则删除S的栈顶元素，用e返回其值，
并返回OK；否则返回ERROR */
Status Pop(LinkStack *S, SElemType *e)
{
	LinkStackPtr p;
	if (StackEmpty(*S))
		return ERROR;
	*e = S->top->data;
	/* 将栈顶结点赋值给p，如图③ */
	p = S->top;
	/* 使得栈顶指针下移一位，指向后一结点，如图④ */
	S->top = S->top->next;
	/* 释放结点p */
	free(p);
	S->count--;
	return OK;
}

