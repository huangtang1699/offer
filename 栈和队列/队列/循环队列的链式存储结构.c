/*
队列（queue）是只允许在一端进行插入操作，而在另一端进行删除操作的线性表。
队列是一种先进先出（First In First Out）的线性表，简称FIFO。
允许插入的一端称为队尾，允许删除的一端称为队头。
*/

/*
ADT 队列(Queue)
Data
	同线性表。元素具有相同的类型，相邻元素具有前驱和后继关系
Operation
	InitQueue(*Q): 初始化操作，建立一个空队列Q。
	DestroyQueue(*Q): 若队列Q存在，则销毁它。
	ClearQueue(*Q): 将队列Q清空。
	QueueEmpty(Q): 若队列Q为空，返回true，否则返回false。
	GetHead(Q, *e): 若队列Q存在且非空，用e返回队列Q的队头元素。
	EnQueue(*Q, e): 若队列Q存在，插入新元素e到队列Q中并成为队尾元素。
	DeQueue(*Q, *e): 删除队列Q中队头元素，并用e返回其值。
	QueueLength(Q): 返回队列Q的元素个数。
endADT
*/

/* 队列为满时的条件(rear+1)%QueueSize==front（取模“%”的目的就是为了整合rear与front大小为一个问题） */
/* (rear-front+QueueSize)%QueueSize */

/* QElemType类型根据实际情况而定，这里假设为int */
typedef int QElemType;
/* 结点结构 */
typedef struct QNode {
	QElemType data;
	struct QNode *next;
} QNode, *QueuePtr;

/* 队列的链表结构 */
typedef struct {
	/* 队头、队尾指针 */
	QueuePtr front, rear;
} LinkQueue;

//入队操作
/* 插入元素e为Q的新的队尾元素 */
Status EnQueue(LinkQueue *Q, QElemType e)
{
	QueuePtr s =
	    (QueuePtr)malloc(sizeof(QNode));
	/* 存储分配失败 */
	if (!s)
		exit(OVERFLOW);
	s->data = e;
	s->next = NULL;
	/* 把拥有元素e新结点s赋值给原队尾结点的后继， */
	Q->rear->next = s;
	/* 见上图中① */
	/* 把当前的s设置为队尾结点，rear指向s，见上图中② */
	Q->rear = s;
	return OK;
}


//出队操作
/* 若队列不空，删除Q的队头元素，用e返回其值，
并返回OK，否则返回ERROR */
Status DeQueue(LinkQueue *Q, QElemType *e)
{
	QueuePtr p;
	if (Q->front == Q->rear)
		return ERROR;
	/* 将欲删除的队头结点暂存给p，见上图中① */
	p = Q->front->next;
	/* 将欲删除的队头结点的值赋值给e */
	*e = p->data;
	/* 将原队头结点后继p->next赋值给头结点后继， */
	Q->front->next = p->next;
	/* 见上图中② */
	/* 若队头是队尾，则删除后将rear指向头结点，见上图中③ */
	if (Q->rear == p)
		Q->rear = Q->front;
	free(p);
	return OK;
}