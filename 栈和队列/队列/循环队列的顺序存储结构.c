/*
队列（queue）是只允许在一端进行插入操作，而在另一端进行删除操作的线性表。
队列是一种先进先出（First In First Out）的线性表，简称FIFO。
允许插入的一端称为队尾，允许删除的一端称为队头。
*/

/*
ADT 队列(Queue)
Data
	同线性表。元素具有相同的类型，相邻元素具有前驱和后继关系
Operation
	InitQueue(*Q): 初始化操作，建立一个空队列Q。
	DestroyQueue(*Q): 若队列Q存在，则销毁它。
	ClearQueue(*Q): 将队列Q清空。
	QueueEmpty(Q): 若队列Q为空，返回true，否则返回false。
	GetHead(Q, *e): 若队列Q存在且非空，用e返回队列Q的队头元素。
	EnQueue(*Q, e): 若队列Q存在，插入新元素e到队列Q中并成为队尾元素。
	DeQueue(*Q, *e): 删除队列Q中队头元素，并用e返回其值。
	QueueLength(Q): 返回队列Q的元素个数。
endADT
*/

/* 队列为满时的条件(rear+1)%QueueSize==front（取模“%”的目的就是为了整合rear与front大小为一个问题） */
/* (rear-front+QueueSize)%QueueSize */

/* QElemType类型根据实际情况而定，这里假设为int */
typedef int QElemType;
/* 循环队列的顺序存储结构 */
typedef struct {
	QElemType data[MAXSIZE];
	/* 头指针 */
	int front;
	/* 尾指针，若队列不空，
	指向队列尾元素的下一个位置 */
	int rear;
} SqQueue;

/* 初始化一个空队列Q */
Status InitQueue(SqQueue *Q)
{
	Q->front = 0;
	Q->rear = 0;
	return OK;
}

/* 返回Q的元素个数，也就是队列的当前长度 */
int QueueLength(SqQueue Q)
{
	return (Q.rear - Q.front + MAXSIZE) % MAXSIZE;
}

/* 若队列未满，则插入元素e为Q新的队尾元素 */
Status EnQueue(SqQueue *Q, QElemType e)
{
	/* 队列满的判断 */
	if ((Q->rear + 1) % MAXSIZE == Q->front)
		return ERROR;
	/* 将元素e赋值给队尾 */
	Q->data[Q->rear] = e;
	/* rear指针向后移一位置， */
	Q->rear = (Q->rear + 1) % MAXSIZE;
	/* 若到最后则转到数组头部 */
	return OK;
}

/* 若队列不空，则删除Q中队头元素，用e返回其值 */
Status DeQueue(SqQueue *Q, QElemType *e)
{
	/* 队列空的判断 */
	if (Q->front == Q->rear)
		return ERROR;
	/* 将队头元素赋值给e */
	*e = Q->data[Q->front];
	/* front指针向后移一位置， */
	Q->front = (Q->front + 1) % MAXSIZE;
	/* 若到最后则转到数组头部 */
	return OK;
}