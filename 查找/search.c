/*****************************************
* 折半查找
* 在折半查找时，首先将输入值x与数组v的中间元素进行比较。
* 如果x小于中间元素的值，则在该数组的前半部分查找；
* 否则，在该数组的后半部分查找。在这两种情况下，
* 下一步都是将x与所选部分的中间元素进行比较。这个过程一直
*进行下去，直到找到指定的值或查找范围为空。
*****************************************/
/* binsearch函数：在v[0]<=v[1]<=v[2]<=...<=v[n-1]中查找x */
int binsearch(int x, int v[], int n)
{
    int low, high, mid;
    
    low = 0；
    high = n -1;
    while(low <= high)
    {
        mid = (low+high) / 2;
        if(x < v[mid])
            high = mid - 1;
        else if(x > v[mid])
            low = mid + 1;         
        else
            return mid；
    }
    return -1；
}
