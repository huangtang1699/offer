/*****************************************
* shell排序
* 先比较距离远的元素，而不是像简单交换排序算法那样先比较相邻的元素。
* 这样可以快速减少大量的无序情况，从而减轻后续的工作。被比较的元素
* 之间的距离逐步减少，直到减少为1，这时排序变成了相邻元素的互换。
*****************************************/

void shellsort( int v[], int n)
{
    int gap, i, j, temp;
    
    for( gap = n/2; gap > 0; gap /= 2)
        for( i = gap; i < n; i++)
            for(j = i - gap; j >= 0 && v[j] > v[j+gap]; j-=gap)
            {
                temp = v[j];
                v[j] = v[j+gap];
                v[j+gap] = temp;
            }
}

/*****************************************
* 快速排序：
* 对于一个给定的数组，从中选择一个元素，以该元素为界将其余元素划分为两个子集，
* 一个子集中的所有元素都小于该元素，另一个子集中的所有元素都大于或等于该元素。
* 对这样两个子集递归执行这一过程，当某个子集中的元素数小于2时，
* 这个子集就不需要再次排序，终止递归。
*****************************************/
/* qsort函数：以递增顺序对v[left] ...v[right]进行排序 */
void qsort(int v[], int left, int right)
{
    int i, last;
    void swap(int v[], int i, int j);
    
    if(left >= right)
        return;
    
    swap(v, left, (left + right)/2);
    last = left;
    for( i=left+1; i <= right; i++ )
        if(v[i] < v[left])
            swap(v, ++last, i);
            
    swap(v, left, last);
    
    qsort(v, left, last-1);
    qsort(v, last+1, right);
}

/* swap函数：交换v[i]与v[j]的值 */
void swap(int v[], int i, int j)
{
    int temp;
    
    temp = v[i];
    v[i] = v[j];
    v[j] = temp;
}

//直接插入法一
void InsertSort1(int a[], int n)
{
    int i, j;
    for(i=1; i<n; i++)
        if(a[i] < a[i-1])   
        {
            int temp = a[i];                       //保存要比较的值
            for(j=i-1; j>=0 && a[j]>temp; j--)    //从后向前查找待插入位置
                a[j+1] = a[j];                    //挪位
            a[j+1] = temp;                       //复制到插入位置
        }
}

//直接插入法二：用数据交换代替法一的数据后移(比较对象只考虑两个元素)
void InsertSort2(int a[], int n)
{
    for(int i=1; i<n; i++)
        for(int j=i-1; j>=0 && a[j]>a[j+1]; j--)
            Swap(a[j], a[j+1]);
}
void Swap(int a,int b){
    int temp;
    temp=a;
    a=b;
    b=temp;
}


//冒泡排序
void BubbleSort(int a[], int n)
{
    int i, j;
    for(i=0; i<n; i++){
        bool flag=false;              //表示本趟冒泡是否发生交换的标志
        for(j=1; j<n-i; j++){         //j的起始位置为1，终止位置为n-i  
            if(a[j]<a[j-1]){
               Swap(a[j-1], a[j]);
                flag=true;
            }
        }
        if(flag==false)             //未交换，说明已经有序，停止排序
            return;
    }          
}
