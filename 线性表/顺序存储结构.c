/*
ADT 线性表(List)
Data
    线性表的数据对象集合为{a1, a2, ......, an}，每个元素的类型均为DataType。
    其中，除第一个元素a1外，每一个元素有且只有一个直接前驱元素，
    除了最后一个元素an外，每一个元素有且只有一个直接后继元素。
    数据元素之间的关系是一对一的关系。
Operation
    InitList(*L):          初始化操作，建立一个空的线性表L。
    ListEmpty(L):          若线性表为空，返回true，否则返回false。
    ClearList(*L):         将线性表清空。
    GetElem(L, i, *e):     将线性表L中的第i个位置元素值返回给e。
	LocateElem(L, e):      在线性表L中查找与给定值e相等的元素,如果查找成功，返回该元素在表中序号表示成功；
*/




/* 存储空间初始分配量 */
#define MAXSIZE 20
/* ElemType类型根据实际情况而定，这里假设为int */
typedef int ElemType;

typedef struct {
	/* 数组存储数据元素，最大值为MAXSIZE */
	ElemType data[MAXSIZE];
	/* 线性表当前长度 */
	int length;
} SqList;

#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int Status;
/* Status是函数的类型，其值是函数结果状态代码，如OK等 */
/* 初始条件：顺序线性表L已存在，1 ≤ i ≤ ListLength(L) */
/* 操作结果：用e返回L中第i个数据元素的值 */
Status GetElem(SqList L, int i, ElemType *e)
{
	if (L.length == 0 || i < 1 ||
	        i > L.length)
		return ERROR;
	*e = L.data[i - 1];
	return OK;
}


/*
插入算法的思路：
如果插入位置不合理，抛出异常；
如果线性表长度大于等于数组长度，则抛出异常或动态增加容量；
从最后一个元素开始向前遍历到第i个位置，分别将它们都向后移动一个位置；
将要插入元素填入位置i处;
表长加1。
*/
/* 初始条件：顺序线性表L已存在，1 ≤ i ≤ ListLength(L) */
/* 操作结果：在L中第i个位置之前插入新的数据元素e，L的长度加1 */
Status ListInsert(SqList *L, int i, ElemType e)
{
	int k;
	/* 顺序线性表已经满 */
	if (L->length == MAXSIZE)
		return ERROR;
	/* 当i不在范围内时 */
	if (i < 1 || i >L->length + 1)
		return ERROR;
	/* 若插入数据位置不在表尾 */
	if (i <= L->length) {
		/*将要插入位置后数据元素向后移动一位 */
		for (k = L->length - 1; k >= i - 1; k--)
			L->data[k + 1] = L->data[k];
	}
	/* 将新元素插入 */
	L->data[i - 1] = e;
	L->length++;
	return OK;
}


/*
删除算法的思路：
如果删除位置不合理，抛出异常；
取出删除元素；
从删除元素位置开始遍历到最后一个元素位置，分别将它们都向前移动一个位置；
表长减1。
*/
/* 初始条件：顺序线性表L已存在，1 ≤ i ≤ ListLength(L) */
/* 操作结果：删除L的第i个数据元素，并用e返回 其值，L的长度减1 */
Status ListDelete(SqList *L, int i, ElemType *e)
{
	int k;
	/* 线性表为空 */
	if (L->length == 0)
		return ERROR;
	/* 删除位置不正确 */
	if (i < 1 || i > L->length)
		return ERROR;
	*e = L->data[i - 1];    /* 如果删除不是最后位置 */
	if (i < L->length) {
		/* 将删除位置后继元素前移 */
		for (k = i; k < L->length; k++)
			L->data[k - 1] = L->data[k];
	}
	L->length--;
	return OK;
}



/* 将所有的在线性表Lb中但不在La中的数据元素插入到La中 */
void unionL(List *La, List Lb)
{
	int La_len, Lb_len, i;
	/* 声明与La和Lb相同的数据元素e */
	ElemType e;
	/* 求线性表的长度 */
	La_len = ListLength(*La);
	Lb_len = ListLength(Lb);
	for (i = 1; i <= Lb_len; i++) {
		/* 取Lb中第i个数据元素赋给e */
		GetElem(Lb, i, &e);
		/* La中不存在和e相同数据元素 */
		if (!LocateElem(*La, e))
			/* 插入 */
			ListInsert(La, ++La_len, e);
	}
}

